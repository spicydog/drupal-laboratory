<?php
/**
 * @file
 * Administration page callbacks for the annotate module.
 */



function ExtAuth_admin_settings() {
  // Get an array of node types with internal names as keys and
  // "friendly names" as values. E.g.,
  // array('page' => 'Basic Page', 'article' => 'Articles')


  $form['ExtAuth_auth_url'] = array(
    '#type' => 'textfield',
    '#title' => t('URL of external authentication web service'),
    '#description' => t('URL of the authentication web service.<br />
      Example: https://api.example.com/account?user={user}&pass={pass}<br />
      {user} is the requested username and {pass} is the requested password.'),
    '#default_value' => variable_get('ExtAuth_auth_url', 'https://api.example.com/account?username={user}&password={pass}'),
    '#size' => 200
  );

  $form['ExtAuth_prefix'] = array(
    '#type' => 'textfield',
    '#title' => t('Prefix of username and email created on this system'),
    '#description' => t('Prefix will be added to the front of username and email
      in order to classify user accounts.<br />'),
    '#default_value' => variable_get('ExtAuth_prefix', 'ext_'),
    '#size' => 7
  );

  $form['ExtAuth_access_role'] = array(
    '#type' => 'textfield',
    '#title' => t('Access Role'),
    '#description' => t('Name of the role on web service which allows to use this system.<br />
      Leave blank if access role is not required.'),
    '#default_value' => variable_get('ExtAuth_access_role', ''),
    '#size' => 20
  );

  $form['#submit'][] = 'ExtAuth_admin_settings_submit';

  return system_settings_form($form);

}


/**
 * Validate annotation settings submission.
 */
function ExtAuth_admin_settings_validate($form, &$form_state) {
  // $limit = $form_state['values']['ExtAuth_limit_per_node'];
  // if (!is_numeric($limit)) {
  //   form_set_error('ExtAuth_limit_per_node', t('Please enter number.'));
  // }
}

/**
* Process annotation settings submission.
*/
function ExtAuth_admin_settings_submit($form, $form_state) {


$auth_url     = variable_set('ExtAuth_auth_url', ($form_state['values']['ExtAuth_auth_url']));
$prefix       = variable_set('ExtAuth_prefix', ($form_state['values']['ExtAuth_prefix']));
$access_role  = variable_set('ExtAuth_access_role', ($form_state['values']['ExtAuth_access_role']));


//drupal_set_message(t('Your configuration has been saved.'));


}
