// Define variables
var map;
var layers = ['BASE','GISTDA_SPOT5','GOOGLE','GOOGLE_EN','GOOGLE_ROAD','GRAY','GRAY_EN','HYDRO','MAPQUEST','NORMAL','NORMAL_EN','OPENCYCLE','POI','POI_EN','POI_TRANSPARENT','POI_TRANSPARENT_EN','POLITICAL','POLITICAL_EN','POLITICAL_NOLABEL','SATELLITE','TERRAIN','THAICHOTE'];




// Initialize the map
function init() {
	map = new longdo.Map({
		placeholder: document.getElementById('map')
	});

	map.Event.bind('overlayClick',function(markerSelect){
		//map.Overlays.remove(markerSelect);
		console.log(markerSelect);
	});

	map.Event.bind('location',function(){
		update_xy();


		if(adding_line.length>0) {
			draw_current_adding_line();
		}


	});

	$('#zoombar').attr('data-slider-value',map.zoom());
	request_owner();

}
init();


// Update Lat and Lon Textbox
function update_xy(){
	$('#lat').val(map.location().lat);
	$('#lon').val(map.location().lon);
}
update_xy();




// Generate layer options
for(var i in layers) {
	var o = '<li id="layer_list_item_'+layers[i]+'" lid="'+i+'" ><a href="#">'+layers[i]+'</a></li>';
    $("#layer_list").append(o);

    $( "[id^=layer_list_item_]" ).click(function() {
    	$('#current_layer').html($(this).html());
		map.Layers.setBase(longdo.Layers[layers[$(this).attr('lid')]]);
	});
}

// Slidebar
$('#zoombar').slider().on('slide', function(ev){
    map.zoom($('#zoombar').val());
});

// Traffic Layer Checkbox
$( "#chk_traffic" ).click(function() {
	if($(this).is(':checked')) {
		map.Layers.add(longdo.Layers.TRAFFIC);
	} else {
		map.Layers.remove(longdo.Layers.TRAFFIC);
	}
});

// Traffic OSM Checkbox
$( "#chk_osm" ).click(function() {
	if($(this).is(':checked')) {
		map.Layers.add(longdo.Layers.OSM);
	} else {
		map.Layers.remove(longdo.Layers.OSM);
	}
});


// Get Current User Location
$('#btn_mylocation').click(function(){
	map.location('GEOLOCATION');
	update_xy();
});

// Jump to Location
$('#btn_gotolocation').click(function(){
	map.location( { lon:$('#lon').val(), lat:$('#lat').val() }, true);

});



// Search
$('#btn_search').click(function(){
  suggestKeyword = null;
  searchKeyword = $('#searchbox').val();
  $('#searchresult').empty();
  var data = {};
  var linktest = '';
  if (searchKeyword) data.keyword = searchKeyword;
  if ($('#lat').val()) data.lat = $('#lat').val();
  if ($('#lon').val()) data.lon = $('#lon').val();

  $('#search_result').html('');
  $.getJSON('http://api.longdo.com/map/services/search?callback=?', data, function(msg) {
  	var result = msg.data;
  	for (var i = 0; i < result.length; i++) {
  		$('#search_result').append( '<a href="#" class="list-group-item" id="result_item" class="result_item" icon="'+result[i].icon+'" lat="'+result[i].lat+'" lon="'+result[i].lon+'">'+result[i].name+'</a>' );

		var marker = new longdo.Marker({ lon:result[i].lon, lat:result[i].lat },{
		  title: 'Marker',
		  icon: {
		    url: 'http://api.longdo.com/map/images/icons/'+result[i].icon,
		    offset: { x: 9, y: 9 }
		  },
		  draggable: true
		});
		map.Overlays.add(marker);
  	};


	$('[id=result_item]').click(function(){
		console.log($(this).html());
		map.location( { lon:$(this).attr('lon'), lat:$(this).attr('lat') }, true);

	});

  });
});


// Add Truck Marker
n_marker = 0;
marker = new Array();
$('#btn_droppin').click(function(){
	marker[n_marker] = new longdo.Marker({ lon:$('#lon').val(), lat:$('#lat').val() },{
	  title: 'Marker',
	  icon: {
	    url: 'http://www.veryicon.com/icon/png/Transport/Perfect%20Transport/Panel%20truck.png',
	    offset: { x: 24, y: 24 }
	  },
	  draggable: true
	});
	map.Overlays.add(marker[n_marker]);
	n_marker++;
});

// Add popup
$('#btn_addpopup').click(function(){
	var title = prompt("Title: ");
	var detail = prompt("Detail: ");
	var popup = new longdo.Popup( map.location(),
	{
	  title: title,
	  detail: detail
	});
	map.Overlays.add(popup);
});





// Add a route
var adding_route = false;
$('#btn_addroute').click(function(){
	if( !adding_route ) {
		map.Overlays.clear();
		adding_route = map.location();
	} else {
		map.Route.placeholder(document.getElementById('route'));
		map.Route.add(adding_route);
		map.Route.add(map.location());

		map.Route.search();
		map.Route.enableContextMenu();
		map.Route.auto(true);
		adding_route = false;
	}
});


// Add a moving truck
var moving_truck = false;
$('#btn_addmovingtruck').click(function(){
	move_truck();
});

function move_truck(){
	map.Overlays.clear();

	// In real situations, we acquire lon and lat from the server here.
	var lon = parseFloat($('#lon').val())+Math.random()*0.1;
	var lat = parseFloat($('#lat').val())+Math.random()*0.1;
	moving_truck = new longdo.Marker({ lon:lon, lat:lat },{
	  title: 'Marker',
	  icon: {
	    url: 'http://www.veryicon.com/icon/png/Transport/Perfect%20Transport/Panel%20truck.png',
	    offset: { x: 24, y: 24 }
	  },
	  draggable: true
	});
	map.Overlays.add(moving_truck);

	setTimeout("move_truck();",2000);
}



function request_owner() {
	$.post('http://127.0.0.1:8888/drupal/mymap_request1', function(data){
		data = JSON.parse(data);

		for(var i in data) {


			if( data[i].region=="NE" )
				data[i].region="I";

			marker[n_marker] = new longdo.Marker({ lon:data[i].longitude, lat:data[i].latitude },{
			  title: data[i].name,
			  clickable: true,
			  icon: {
			    url: 'http://icons.iconarchive.com/icons/hydrattz/multipurpose-alphabet/24/Letter-'+data[i].region+'-blue-icon.png',
			    offset: { x: 24, y: 24 },
			  },
			  popup: {
				  title: data[i].name,
				  detail: 'จังหวัด: ' + data[i].province_code + '<br />ชื่อย่อ: ' + data[i].province_code + '<br />ภาค: ' + data[i].region + '<br />latitude: ' + data[i].latitude + '<br />longitude: ' + data[i].longitude,
				  size: { width: 200, height: 100 },
				  closable: true
				}
			});
			map.Overlays.add(marker[n_marker]);
			n_marker++;

		}
	});
}



var roads = [];

// Search
$('#btn_searchroad').click(function(){
  suggestKeyword = null;
  searchKeyword = $('#searchroadbox').val();
  $('#searchroad_result').empty();
	roads = [];

  $.post('http://127.0.0.1:8888/drupal/mymap_request2',{keyword:searchKeyword}, function(data) {
    var result=JSON.parse(data);

    for (var i = 0; i < result.length; i++) {

    	//console.log(result[i]);
      $('#searchroad_result').append( '<div><a id="resultroad_item" href="#" lineid="'+i+'">'+result[i].route_name+'</a></div>' );
      roads[i] = result[i].st_asgeojson;
    }


    $('[id=resultroad_item]').click(function(){
    	var data = JSON.parse(roads[$(this).attr('lineid')]);
    	var line = [];
    	for (var i = 0; i < data.coordinates.length; i++) {
    		//console.log( data.coordinates[i][0] + ',' +  data.coordinates[i][1] + '\n' + data.coordinates[i+1][0] + ',' +  data.coordinates[i+1][1]);
    		line[i] = { lon:data.coordinates[i][0], lat:data.coordinates[i][1]};

    	};

    		map.Overlays.clear();


	    	var geom1 = new longdo.Polyline(line,{
	    			title: $(this).html()
	    		});
				map.Overlays.add(geom1);

				map.location( { lon:data.coordinates[0][0], lat:data.coordinates[0][1] }, true);


    //map.location( { lon:$(this).attr('lon'), lat:$(this).attr('lat') }, true);

    });

  });

});


// Add a line
var adding_line = [];
var added_line = [];
$('#btn_addline').click(function(){
	adding_line[adding_line.length] = map.location();
});

var adding_polyline;
function draw_current_adding_line() {
	map.Overlays.remove(adding_polyline);
	adding_polyline = new longdo.Polyline(adding_line.concat(map.location()),{
		lineColor: 'rgba(0, 255, 0, 0.8)',
	});
	map.Overlays.add(adding_polyline);
}

$('#btn_finishaddline').click(function(){
	draw_current_adding_line();

	adding_line[adding_line.length] = map.location();
	added_line = adding_line.slice(0);
	adding_line = [];

	var data = new Object();
	data.line = added_line;
	data.title = prompt("Title: ");

	$.post('http://127.0.0.1:8888/drupal/mymap_request3',{data:data}, function(data) {

	});





});







